package vendingMachine;

public class Item {

	private String name;
	private int price;
	private int boxCategory;
	private int boxContents;

	public Item(String name, int price, int category, int contents) {
		this.name = name;
		this.price = price;
		this.boxCategory = category;
		this.boxContents = contents;

	}

	public String info() {
		return this.name + ":" + this.price + "円";
		//後で訂正
	}

	public String getName() {
		return this.name;
	}

	public int getPrice() {
		return this.price;

	}

	public int getBoxContents() {
		return this.boxContents;
	}

	public int getBoxCategory() {
		return this.boxCategory;
	}

}
