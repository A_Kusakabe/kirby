package vendingMachine;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Human {

	public static void main(String[] args) {

		KeyIn ki = new KeyIn();
		String select = "0";
		int selectItem = 0;
		String registerFlag = "2";
		String m = null;

		Hanbaiki kenbaiki = new Hanbaiki();

		kenbaiki.viewMenu();
		System.out.println();

		System.out.println("1.現金で払う");
		System.out.println("2.iDで決済");
		System.out.println("3.その他の電子マネーを使う");
		String pay = ki.readString("決済方法を選択してください:");
		if (pay.equals("")) {
			pay = "1";
			System.out.println();
			System.out.println("現金支払いを自動的に選択しました。");

		}
		int pay2 = Integer.parseInt(pay);

		switch (pay2) {

		case 1: //現金

			m = ki.readString("現金投入:");
			System.out.println(m);
			if (m.equals("")) {
				m = "0";
				System.out.println("お金が入っていません。");
				System.exit(0);
			}
			int money = Integer.parseInt(m);
			kenbaiki.buyMenu(money);

			select = ki.readString("商品番号入力:");
			if (select.equals("")) {
				select = "0";
				System.out.println();
				System.out.println("商品選択に誤りがありました。");
				System.exit(0);
			}
			selectItem = Integer.parseInt(select);
			if (selectItem >= 1 && selectItem < 6) {
				kenbaiki.setArray(selectItem);
				kenbaiki.buy(selectItem, money);
			} else {
				System.out.println("商品選択に誤りがあります。");
			}

			break;

		case 2: //iD

			select = ki.readString("商品番号入力:");
			if (select.equals("")) {
				select = "0";
				System.out.println();
				System.out.println("商品選択に誤りがありました。");
				System.exit(0);
			}
			selectItem = Integer.parseInt(select);
			if (selectItem >= 1 && selectItem < 6) {
				kenbaiki.setArray(selectItem);

				System.out.println();
				System.out.print("電子マネーをかざす(10秒以内):[1]");

				TimerTask task = new TimerTask() {
					public void run() {
						System.out.println("決済がタイムアウトしました。");
						System.exit(0);
					}
				};
				Timer timer = new Timer();
				timer.schedule(task, 10000L);

				while (true) {

					registerFlag = ki.readString();

					if (!(registerFlag.equals("1"))) {
						System.out.println("1を入力して、電子マネーをかざしてください。");
						System.out.println("もう一度かざしてください：>");
					} else {
						break;
					}
				}
				int registerFlag2 = Integer.parseInt(registerFlag);

				if (registerFlag2 == 1) {
					System.out.println("決済完了");
					kenbaiki.itemOut(selectItem);
					System.exit(0);
				} else {

					System.out.println("決済失敗");
				}

				//電子マネーを読み取れれば決済終了、商品排出
			} else {
				System.out.println("商品選択に誤りがあります。");
			}

			break;

		case 3: //Suica
			select = ki.readString("商品番号入力:");
			if (select.equals("")) {
				select = "0";
				System.out.println();
				System.out.println("商品選択に誤りがありました。");
				System.exit(0);
			}
			selectItem = Integer.parseInt(select);
			if (selectItem >= 1 && selectItem < 6) {
				kenbaiki.setArray(selectItem);
				kenbaiki.suica(selectItem);
			} else {
				System.out.println("商品選択に誤りがあります。");
			}

			break;
		default:
			System.out.println("お支払い方法の選択に誤りがあります。");
			break;
		}

	}

	public void timer_delay() throws InterruptedException {
		TimerTask task = new SampleTask();
		Timer timer = new Timer("遅延タイマー");

		System.out.println("main start：" + new Date());
		timer.schedule(task, TimeUnit.SECONDS.toMillis(20)); //10秒後に実行

		TimeUnit.SECONDS.sleep(30); //30秒間待つ
		timer.cancel();
		System.out.println("main end  ：" + new Date());
	}

}

class SampleTask extends TimerTask {

	/** このメソッドがTimerから呼ばれる */
	@Override
	public void run() {
		System.out.println("タスク実行：" + new Date());
	}
}
