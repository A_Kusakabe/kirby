﻿package vendingMachine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileIn{
    BufferedReader br = null; //BufferedReaderクラス
    public boolean open(String fname){
        boolean sts = true;
        try{
            br = new BufferedReader(new FileReader(fname));
        }catch(IOException e){
            System.out.println("ファイル名に誤りがあります\n" + e);
            sts = false;
        }
        return sts;
    }

    public String readLine(){
         String buff;
         try{
             buff = br.readLine();
         }catch(IOException e){
             System.out.println("読み込みエラ－\n" + e);
             buff = null;
        }
        return buff;
    }

    public boolean close(){
        boolean sts = true;
        try{
            br.close();
        }catch(IOException e){
            System.out.println("ファイルクローズエラ－\n" + e);
            sts = false;
        }
        return sts;
    }
}
