﻿package vendingMachine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KeyIn {
	String buf = null; // ① 入力バッファの初期化
	BufferedReader br = // ② キーボード入力の準備
			new BufferedReader(new InputStreamReader(System.in));

	/* 文字列を入力するメソッド*/
	public String readString() {
		try {
			buf = br.readLine(); // ③ キーボード入力
		} catch (IOException e) { //    キーボード入力致命的エラー
			System.out.println(e); //    エラー情報の表示
			System.exit(1); //    プログラムの異常終了
		}
		return buf; // ④ 文字列を返却
	}

	/* 入力プロンプトを表示して文字列を入力するメソッド */
	public String readString(String msg) {
		System.out.print(msg + ">"); // ⑤ プロンプト表示
		return readString(); // ⑥ キーボード入力
	}

	/* 整数値を入力するメソッド*/

	public int readInt() {
		int inputIntValue;
		while (true) {
			buf = readString();
			try {
				inputIntValue = Integer.parseInt(buf); // 数値に変換
				break; // ループ終了
			} catch (NumberFormatException e) { // 数値変換のエラー
				System.out.println("整数値を入力してください：" + buf);
				System.out.print("再入力>");
			}
		}
		return inputIntValue; // 変換した値の返却
	}

	/* 入力プロンプトを表示して整数値を入力するメソッド */
	public int readInt(String msg) {
		System.out.print(msg + ">"); // プロンプトを表示
		return readInt(); // readInt()の呼び出し
	}

}
