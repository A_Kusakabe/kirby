package vendingMachine;

public class Hanbaiki {

	private int itemCount;
	private Item[] itemArray;
	private String boxArray[] = new String[3];
	private int index = (int) (Math.random() * 2);

	public Hanbaiki() {
		this.itemCount = 0;
		this.itemArray = new Item[5];

		this.boxArray = new String[3];

		Item Item = new Item("おぞましいもの", 100, 1, index);
		setItem(Item);
		Item Item2 = new Item("こわいもの", 200, 2, index);
		setItem(Item2);
		Item Item3 = new Item("かっこいいもの", 400, 3, index);
		setItem(Item3);
		Item Item4 = new Item("すごいもの", 500, 4, index);
		setItem(Item4);
		Item Item5 = new Item("かわいいもの", 700, 5, index);
		setItem(Item5);
	}

	public void buy(int number, int totalMoney) {

		String name = this.itemArray[number - 1].getName();
		int gold = this.itemArray[number - 1].getPrice();
		if (totalMoney >= gold) {
			System.out.println(name + "を購入しました。");
			totalMoney = totalMoney - gold;
			System.out.println("おつり:" + totalMoney);
			System.out.println();
			System.out.println("中身は[ " + this.boxArray[this.index] + " ]だった。");
		} else {
			System.out.println("投入金額が足りません");
		}
	}

	public void viewMenu() {
		for (int i = 0; this.itemCount > i; i++) {

			Item Item = this.itemArray[i];

			System.out.println((i + 1) + "." + Item.info());

		}

	}

	public void buyMenu(int money) {
		for (int i = 0; this.itemCount > i; i++) {

			if (this.itemArray[i].getPrice() <= money) {

				Item Item = this.itemArray[i];

				System.out.println((i + 1) + "." + Item.info());
			}
		}

	}

	public void setArray(int category) {

		switch (category) {
		case 1:
			boxArray[0] = "さとうりょう（推定1円）";
			boxArray[1] = "うしのふん";
			boxArray[2] = "ブラウン管テレビ（ひたすらにでかい）";
			break;
		case 2:
			boxArray[0] = "能面のお面（推定200円）";
			boxArray[1] = "シオハタヨウヘイ（※武器は装備しないと意味ないぞ）";
			boxArray[2] = "浮気の証拠（3点セット）";

			break;
		case 3:

			boxArray[0] = "金色の剣（推定200円）";
			boxArray[1] = "やっすん（価値なし）";
			boxArray[2] = "無間匤煉剩祓 破耶武装式・三核外装\r\n" +
					"（むげんきょくれんじょうのはらえ　はじゃぶそうしき・みかくがいそう）\r\n" +
					"";
			break;
		case 4:
			boxArray[0] = "はやかわかずま（釧路からお届けします）";
			boxArray[1] = "PS4 プレイステーツョン（パチモン）";
			boxArray[2] = "仕天堂Switch（パチモン）";
			break;
		case 5:
			boxArray[0] = "ミッ〇ーのぬいぐるみ（ネズミーランドのやつ）";
			boxArray[1] = "ハンドバッグ（なんか汚れてるけど…）";
			boxArray[2] = "さとりょうの前髪（写真付き）";
			break;

		}

	}

	public void setItem(Item Item) {
		this.itemArray[this.itemCount++] = Item;

	}

	public void id(int number) {
		String name = this.itemArray[number - 1].getName();
		System.out.println(name + "を購入しました。");

	}

	public void suica(int number) {

		String name = this.itemArray[number - 1].getName();
		int gold = this.itemArray[number - 1].getPrice();

		FileIn fi = new FileIn();
		String fileName = "credit.csv";
		String bufin = "";

		FileOut fo = new FileOut();
		String[] strArray = new String[100];
		int strArrayCount = 0;

		fi.open(fileName);

		bufin = fi.readLine();

		int n = Integer.parseInt(bufin);

		if (n < gold) {
			//何もしない
			System.out.println("残高不足です");

		} else {

			n = n - gold;
			int num = n;
			Integer oi = new Integer(n);

			String suica = oi.toString();

			bufin = suica;
			System.out.println(name + "を購入しました。");
			System.out.println("ポイント残高:" + num);
			System.out.println();
			System.out.println("中身は[ " + this.boxArray[this.index] + " ]だった。");

		}
		strArray[strArrayCount] = bufin;
		strArrayCount++;

		//ファイルクローズ
		fi.close();
		fo.open(fileName);
		for (int i = 0; i < strArrayCount; i++) {
			fo.writeln(strArray[i]);
		}
		fo.close();

	}

	public void itemOut(int number) {
		String name = this.itemArray[number - 1].getName();
		System.out.println(name + "が出てきた。");
		System.out.println();
		System.out.println("中身は[ " + this.boxArray[this.index] + " ]だった。");

	}
}
